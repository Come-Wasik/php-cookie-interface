# [PHP] Interface to managing cookies

![PHP](https://img.shields.io/packagist/php-v/nolikein/cookie-interface) ![Packagist version](https://img.shields.io/packagist/v/nolikein/cookie-interface) ![Packagist License](https://img.shields.io/packagist/l/nolikein/cookie-interface)

## Overview

Managing cookies may be difficult without any constraint. So, in the spirit of the [PSR (PHP Standards Recommendations)](https://www.php-fig.org/psr/) I created this interface.
It implements all of what you need to create a cookie class.



## Installation

```bash
composer require nolikein/cookie-interface ^1.0.0
```

## Usage

Create a class like :
```php

use Nolikein\Cookie\CookieInterface;

class Cookie implements CookieInterface
// ...
```

## Links

+ [Link to packagist](https://packagist.org/packages/nolikein/cookie-interface)