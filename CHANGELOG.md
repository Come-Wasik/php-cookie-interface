# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 1.0.3 - 2020-11-01

Add two methods:
+ withSameSite : To add the samesite value
+ getSameSite : To get the samesite value
The "samesite" value can be added in the php native setcookie function. For the moment, it can be 'None', 'Lax' or 'Strict'.
For more precision, see the [Mozilla website](https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Set-Cookie/SameSite).

## 1.0.2 - 2020-11-01
Update the getExpirationTime method: now it can return null which mean that the cookie has no expiration time.

## 1.0.1 - 2020-11-01

Modify the withValue method: add the forgotten parameter.

## 1.0.0 - 2020-10-25

Initial stable release.