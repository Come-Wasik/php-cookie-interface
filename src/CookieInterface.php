<?php

namespace Nolikein\Cookie;

/**
 * Cookie is a web browser element which save data across the pages and across the time.
 * 
 * This interface describe how a cookie must be constructed to be used by the native
 * setcookie function. However any class implementing it, to be generic, MUST NOT use
 * setcookie.
 * 
 * This interface implements all parameters of the setcookie function, due of a
 * compatibility with it.
 * @see https://www.php.net/manual/en/function.setcookie.php
 * 
 * The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
 * "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to
 * be interpreted as described in RFC 2119.
 * @see https://tools.ietf.org/html/rfc2119
 */
interface CookieInterface
{
    /**
     * Return a CookieInterface instance with the name given as parameter.
     * 
     * @param string $name The name of the cookie.
     * 
     * @return CookieInterface A copy of the current instance.
     */
    public function withName(string $name): CookieInterface;

    /**
     * Return a CookieInterface instance with the value given as parameter.
     * 
     * @param string $value The value of the cookie.
     * 
     * @return CookieInterface A copy of the current instance.
     */
    public function withValue(string $value): CookieInterface;

    /**
     * Return a CookieInterface instance with the expiration time given as parameter.
     * 
     * If the time given is a int, it MUST be a Unix timestamp so is in number
     * of seconds since the epoch. You'll most likely set this with the time()
     * function plus the number of seconds before you want it to expire. Or you
     * might use mktime(). time()+60*60*24*30 will set the cookie to expire in 30
     * days. If set to 0, or omitted, the cookie will expire at the end of the
     * session (when the browser closes).
     * 
     * You may notice the expires parameter takes on a Unix timestamp, as opposed
     * to the date format Wdy, DD-Mon-YYYY HH:MM:SS GMT, this is because PHP does
     * this conversion internally. 
     * 
     * You SHOULD convert the int and register it as a Datetime before register it.
     * 
     * @param \Datetime|int $expirationTime The time when the cookie will be expired.
     * 
     * @return CookieInterface A copy of the current instance.
     * 
     * @throws InvalidArgumentException The $expirationTime argument is neither
     * an int and a Datetime object.
     */
    public function withExpirationTime($expirationTime): CookieInterface;

    /**
     * Return a CookieInterface instance with the path given as parameter.
     * 
     * A path given as parameter MUST begin by a '/'. If is not, the object
     * implementing this interface HAVE TO add a '/' at the beginning.
     * 
     * If the $path parameter is set to '/', the cookie will be available within
     * the entire domain. If set to '/foo/', the cookie will only be available
     * within the /foo/ directory and all sub-directories such as /foo/bar/ of domain.
     * 
     * The default value is the current directory that the cookie is being set in.
     * 
     * @param string $path The path on the server in which the cookie will be available on.
     * 
     * @return CookieInterface A copy of the current instance.
     */
    public function withPath(string $path): CookieInterface;

    /**
     * Return a CookieInterface instance with the domain given as parameter.
     * 
     * Setting the $domain parameter to a subdomain (such as 'www.example.com')
     * will make the available to that subdomain and all other sub-domains of
     * it (i.e. w2.www.example.com).
     * 
     * To make the cookie available to the whole domain (including all
     * subdomains of it), simply set the value to the domain name ('example.com',
     * in this case). 
     * 
     * @param string $domain The (sub)domain that the cookie is available to.
     * 
     * @return CookieInterface A copy of the current instance.
     */
    public function withDomain(string $domain): CookieInterface;

    /**
     * Return a CookieInterface instance with a secure cookie and a http only
     * cookie given as parameter.
     * 
     * When $isSecure is set to TRUE, the cookie will only be set if a secure
     * connection exists. On the server-side, it's on the programmer to send
     * this kind of cookie only on secure connection (e.g. with respect
     * to $_SERVER["HTTPS"]).
     * 
     * When $isHttpOnly is set to TRUE, it means that the cookie won't be
     * accessible by scripting languages, such as JavaScript. It has been
     * suggested that this setting can effectively help to reduce identity
     * theft through XSS attacks (although it is not supported by all browsers),
     * but that claim is often disputed.
     * 
     * @param bool $isSecure Indicates that the cookie should only be transmitted over a secure HTTPS connection from the client.
     * @param bool $isHttpOnly When TRUE the cookie will be made accessible only through the HTTP protocol.
     * 
     * @return CookieInterface A copy of the current instance.
     */
    public function withSecurity(bool $isSecure, bool $isHttpOnly = false): CookieInterface;

    /**
     * Return a CookieInterface instance with a sameSite value given as parameter.
     * 
     * The samesite value MUST be a string and its value can be 'None', 'Lax'
     * or 'Strict'.
     * 
     * @param string $sameSite Tell the samesite constraint.
     * 
     * @return CookieInterface A copy of the current instance.
     * 
     * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Set-Cookie/SameSite
     */
    public function withSameSite(string $sameSite): CookieInterface;

    /**
     * Gets the name of the cookie
     * 
     * @return string The name of the cookie.
     */
    public function getName(): string;

    /**
     * Gets the value of the cookie.
     * 
     * @return string The value the the cookie.
     */
    public function getValue(): string;

    /**
     * Gets the expiration time of the cookie.
     * 
     * @return \Datetime|null A Datetime with the expiration time
     * or null if there is no expiration time.
     */
    public function getExpirationTime(): ?\DateTime;

    /**
     * Gets a boolean with TRUE or FALSE depends of if the cookie has expired.
     * 
     * @return bool TRUE if the cookie has expired, FALSE if not.
     */
    public function isExpired(): bool;

    /**
     * Gets the path on the server in which the cookie will be available on.
     * 
     * @return string The current path.
     */
    public function getPath(): string;

    /**
     * Gets the (sub)domain that the cookie is available to.
     * 
     * @return string The current domain.
     */
    public function getDomain(): string;

    /**
     * Gets a boolean with TRUE of FALSE depends of if the cookie will be
     * transmitted over a secure HTTPS connection from the client.
     * 
     * @return bool True if the cookie is secure, FALSE if not.
     */
    public function isSecure(): bool;

    /**
     * Gets a boolean with TRUE of FALSE depends of if the cookie is
     * accessible only through the HTTP protocol.
     * 
     * @return bool True if the cookie is accessible only by HTTP, FALSE if not.
     */
    public function isHttpOnly(): bool;

    /**
     * Gets the samesite value. Can be 'None', 'Lax' or 'Strict'.
     * 
     * @return string The samesite value.
     * 
     * @see https://developer.mozilla.org/fr/docs/Web/HTTP/Headers/Set-Cookie/SameSite
     */
    public function getSameSite(): string;
}
